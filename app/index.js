import clock from "clock";
import document from "document";
import { preferences } from "user-settings";
import * as util from "../common/utils";
import { HeartRateSensor } from "heart-rate";
import { today, goals } from "user-activity";
import { me as device } from "device";
import * as simpleSettings from "./simple/device-settings";

var hrm = new HeartRateSensor();

// Update the clock every minute
clock.granularity = "minutes";

const time = document.getElementById("time");

const hrLabel = document.getElementById("hrLabel");
const stepsRect = document.getElementById("stepsRect");
const caloriesRect = document.getElementById("caloriesRect");

if (device.screen.height == 250) {
  caloriesRect.x = 164;
  caloriesRect.y = 113;
  caloriesRect.height = 4;
}

if (device.screen.height == 250) {
  stepsRect.x = 86;
  stepsRect.y = 170;
  stepsRect.height = 4;
}

const vaultboy = document.getElementById("vaultboy");
const statusSelected = document.getElementById("statusSelected");
statusSelected.style.visibility = "visible";
const generalSelected = document.getElementById("generalSelected");

const activeMinutesStat = document.getElementById("activeMinutesStat");
const caloriesStat = document.getElementById("caloriesStat");
const distanceStat = document.getElementById("distanceStat");
const elevationGainStat = document.getElementById("elevationGainStat");
const stepsStat = document.getElementById("stepsStat");

const statsLabels = document.getElementsByClassName("statsLabel");
const statsInfos = document.getElementsByClassName("statsInfo");
const interfaceElements = document.getElementsByClassName("interface");

let i;
for (i = 0; i < statsLabels.length; i++) {
  statsLabels[i].style.visibility = "hidden";
}
  
for (i = 0; i < statsInfos.length; i++) {
  statsInfos[i].style.visibility = "hidden";
}

let generalButton = document.getElementById("generalButton");
generalButton.onactivate = function(evt) {
  vaultboy.style.visibility = "hidden";
  caloriesRect.style.visibility = "hidden";
  stepsRect.style.visibility = "hidden";
  statusSelected.style.visibility = "hidden";
  generalSelected.style.visibility = "visible";
  
  for (i = 0; i < statsLabels.length; i++) {
    statsLabels[i].style.visibility = "visible";
  }
  
  for (i = 0; i < statsInfos.length; i++) {
    statsInfos[i].style.visibility = "visible";
  }
}

let statusButton = document.getElementById("statusButton");
statusButton.onactivate = function(evt) {
  vaultboy.style.visibility = "visible";
  caloriesRect.style.visibility = "visible";
  stepsRect.style.visibility = "visible";
  statusSelected.style.visibility = "visible";
  generalSelected.style.visibility = "hidden";
  
  for (i = 0; i < statsLabels.length; i++) {
    statsLabels[i].style.visibility = "hidden";
  }
  
  for (i = 0; i < statsInfos.length; i++) {
    statsInfos[i].style.visibility = "hidden";
  }
}

// Declare an event handler that will be called every time a new HR value is received.
hrm.onreading = function() {
  // Peek the current sensor values
  hrLabel.text = `HR ${hrm.heartRate}`;
}

// Begin monitoring the sensor
hrm.start();

let widthMultiplier = 39;
if (device.screen.height == 250) {
  widthMultiplier = 33;
}

// Update the <text> element every tick with the current time
clock.ontick = (evt) => {
  let todayz = evt.date;
  let hours = todayz.getHours();
  let day = todayz.getDate();
  let month = todayz.getMonth();
  if (preferences.clockDisplay === "12h") {
    // 12h format
    hours = hours % 12 || 12;
  } else {
    // 24h format
    hours = util.zeroPad(hours);
  }
  let mins = util.zeroPad(todayz.getMinutes());
  time.text = `${util.zeroPad(day)}.${util.zeroPad(month+1)}, ${hours}:${mins}`;
  
  
  let stepsWidth = (today.local.steps / goals.steps) * widthMultiplier;
  if (stepsWidth > widthMultiplier) { stepsWidth = widthMultiplier; }
  let caloriesWidth = (today.local.calories / goals.calories) * widthMultiplier;
  if (caloriesWidth > widthMultiplier) { caloriesWidth = widthMultiplier; }
  stepsRect.width = stepsWidth;
  caloriesRect.width = caloriesWidth;
}

// This function updates the labels on the display
function updateDisplay() {
  activeMinutesStat.text = today.local.activeMinutes;
  caloriesStat.text = today.local.calories;
  distanceStat.text = today.local.distance;
  elevationGainStat.text = today.local.elevationGain;
  stepsStat.text = today.local.steps;
}

// And update the display every second
setInterval(updateDisplay, 1000);

function settingsCallback(data) {
  if (!data) {
    return;
  }
  if (data.color) {
    for (i = 0; i < interfaceElements.length; i++) {
      interfaceElements[i].style.fill = data.color;
    }
  }
}
simpleSettings.initialize(settingsCallback);
